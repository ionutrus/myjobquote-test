<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    protected $fillable = [
        'url_id'
    ];

    public function url()
    {
        return $this->belongsTo('App\Url', 'url_id', 'id');
    }
}
