<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UrlPost;
use App\Url\UrlManager;
use App\Http\Controllers\Controller;

class UrlController extends Controller
{
    /**
     * Store a newly created link
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UrlPost $request)
    {
        return response()->json(UrlManager::create($request), 201);
    }

    /**
     * Display the specified link.
     *
     * @param  string  $code
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        return response()->json(UrlManager::getUrlByCode($code), 200);
    }
}
