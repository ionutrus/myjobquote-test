<?php

namespace App\Http\Controllers;

use App\Events\OnRedirect;
use App\Url\UrlManager;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index()
    {
        return view('app');
    }

    public function redirect($code)
    {
        // get the url object from db
        $url = UrlManager::getUrlByCode($code, true);

        // create event before redirecting
        event(new OnRedirect($url));

        // do the actual redirecting
        return redirect($url->url);
    }
}
