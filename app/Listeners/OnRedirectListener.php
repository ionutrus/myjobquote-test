<?php

namespace App\Listeners;

use App\Redirect;
use Illuminate\Contracts\Queue\ShouldQueue;

class OnRedirectListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Redirect::create([
            'url_id' => $event->url->id
        ]);
    }
}
