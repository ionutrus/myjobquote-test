<?php

namespace App\Url;

use App\Http\Requests\UrlPost;
use App\Url;

class UrlManager {

    public static function create(UrlPost $request)
    {
        return Url::create(array_merge(
            $request->all(),
            [
                'code' => self::getUniqueCode()
            ]
        ));
    }

    public static function getUniqueCode()
    {
        $generatedCode = str_random(5);
        $existingEntry = self::getUrlByCode($generatedCode, false);

        if (is_null($existingEntry)) {
            return $generatedCode;
        } else {
            return self::getUniqueCode();
        }
    }

    public static function getUrlByCode($code, $fail = true)
    {
        return ($fail) ? Url::where('code', $code)->firstOrFail() : Url::where('code', $code)->first();
    }
}