<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $fillable = [
        'url',
        'code'
    ];

    protected $hidden = [
        'id',
        'code',
        'created_at',
        'updated_at'
    ];

    protected $appends = [
        'origin'
    ];

    public function redirects()
    {
        return $this->hasMany('App\Redirect', 'url_id', 'id');
    }

    public function getOriginAttribute()
    {
        return url()->route('redirect', [
            'code' => $this->code
        ]);
    }
}
