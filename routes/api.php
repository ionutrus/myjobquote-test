<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['guest']
], function() {
    Route::post('urls', 'Api\UrlController@store')->name('urls.post');
    Route::get('urls/{code}', 'Api\UrlController@show')->name('urls.get');
});
